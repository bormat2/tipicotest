import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducers';


const middleware = [/*thunk*/]

// allows debugging in chrome with redux tab
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, /* preloadedState, */
    composeEnhancers(applyMiddleware(...middleware))
)
export default store;

