import withStyles from 'react-jss'
import { connect } from 'react-redux';


// do transformation on props to have usefull object
const getUtilsFromComp = function(compInstance){
    const allClasses = compInstance.props.classes
    // like getClasses will throw an error
    // if we use a class that has not been declare in JSS
    // otherwise it return equivalents class for mat-ui
    const getClasses = function(...classes){
        let c = ''
        for(let clas of classes){
            if(!allClasses[clas]){
                throw Error('class '+ clas + ' does not exist')
            }
            c += allClasses[clas] + ' '
        }
        return c
    }

    return {getClasses}
}


// connect the componenet to redux and add style
const connectWithStyle = ({comp, styles,mapStateToProps, mapDispatchToProps,mergeProps,options} )=>{
    let comp2 = connect(mapStateToProps,mapDispatchToProps,mergeProps,options)(comp)
    return withStyles(styles)(comp2)
}

const addStyle = ({comp, styles}) => {
    return withStyles(styles)(comp)
}

// safeGet allow to get obj[prop1][prop2][prop...][propN]
// if a property does not exist there is no error and undefined is return
const safeGet = (obj, ...props) => {
    let res = obj, undef
    for(let prop of props){
        res = res[prop]
        if(res === undef){
            break
        }
    }
    return res
}

export {connectWithStyle, getUtilsFromComp, safeGet, addStyle}