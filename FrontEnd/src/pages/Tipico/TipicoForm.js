import PropTypes from 'prop-types';
import React from 'react';
import Button from '@material-ui/core/Button';
import { Field, reduxForm } from 'redux-form'
import { TextField} from 'redux-form-material-ui';
import * as formActionCreator from '../../actions/myFormActionCreator'
import {connectWithStyle, getUtilsFromComp, safeGet} from '../../utils.js'

const styles = theme => {
    const paddingLeft = theme.normalPadding+'px'
    const style = {
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing.unit,
            paddingRight: 20 + theme.spacing.unit,
            width: 250,
            marginTop: '20px',
            maxHeight: '47px'
        },
        button: {
            margin: theme.spacing.unit,
        },
        fill:{
            width: '100%',
            // background: 'red'
        },
        flex_element: {
            display: 'flex',
        },
        half_width:{
            'flexGrow': 1,
            'flexShrink': 0,
            'flex-basis': 0, //otherwise change select change of value the size change
        },
        paddingLeft: {
            // background:'blue',
            paddingLeft,
        },
        limitHeight:{
            height: '100px',
            minHeight: '1px',
            // background: 'orange'
        },
        marginTop: {
            marginTop: '20px'
        }
    }
    // debugger
    Object.assign(style,{
        halfInput : {...style.half_width, ...style.paddingLeft},
    })
    console.log(style)
    return style
}

class TypicoForm extends React.Component {
    render() {
        const {handleSubmit, submitting, funcs} = this.props
        const {reset} = funcs
        const {getClasses: cl} = getUtilsFromComp(this)
        return (
            <form onSubmit={handleSubmit} >
                <div className={cl('paddingLeft')}> Enter numbers and we will format them </div>
                <div id="toto0" className={cl('container')}>
                    <div className={cl('fill')}>
                        <Field name="digits" component={TextField} className={cl('textField','fill')}  label="Numbers separate by , " />
                    </div>
                    <div className={cl('fill')}>
                        <Field name="maxCells" component={TextField} className={cl('textField','fill')}  label="Max cells by line" />
                    </div>
                </div>
                 <div className={cl('fill','marginTop')}>
                    <Button variant="contained" type="submit" color="primary" className={cl('button')} disabled={submitting}>
                        display
                    </Button>
                    <Button variant="contained" color="primary" className={cl('button')} onClick={() => reset()}>Reset</Button>
                </div>
            </form>
        );
    }
}

TypicoForm.propTypes = {
    classes: PropTypes.object.isRequired,
    funcs: PropTypes.object.isRequired
};

TypicoForm = reduxForm({
    form: 'reduxForm_card_information',
    enableReinitialize: true,
    validate: (values) => {
        console.log(values)
        const error = {}
        if(isNaN(values.maxCells)){
            error.maxCells = "Only digits are allowed"
        }

        if(values.digits && isNaN(values.digits.split(',').join(''))){
            error.digits =  "Only digits and coma are allowed"
        }
        return error
    }
})(TypicoForm)


// mapStateToProps: each time the store change,
// mapStateToProps is called, this function merge ownProps 
// and store information into props that the component will receive
const mapStateToProps = (state /*of the store, ownprops*/ ) => {
    return {
        formVal: safeGet(state,'form','reduxForm_card_information','values') || {defaultVal:1},
        initialValues: state.myFormReducer.dataForLoading//automatically populate the form at loading
    }
}

const mapDispatchToProps = function(dispatch){
    const {reset} = formActionCreator
    // action witch are synchrone
    const sync = {
        // It seems this prop already exists in props it is why we will encapsulate functions in the 'funcs' property where reset will not be erased
        reset
    }

    // automatically dispatch functions witch are synchrone
    const obj = Object.entries(sync).reduce((o,[name, func]) => {
        if(o.hasOwnProperty(name)){
            throw Error('This prop already exists')
        }
        o[name] = (...args) => dispatch(func(...args))
        return o
    }, {})
    Object.assign(obj, { /* now add asynchrone action */})

    return {
        funcs: obj
    }
}

export default connectWithStyle({comp: TypicoForm, mapStateToProps,
    mapDispatchToProps, styles});