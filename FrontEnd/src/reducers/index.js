import { combineReducers } from 'redux';
import { reducer as reducerForm } from 'redux-form';
import myFormReducer from './myFormReducer';
const rootReducer = combineReducers({
    form: reducerForm,
    myFormReducer,
})
export default rootReducer;
