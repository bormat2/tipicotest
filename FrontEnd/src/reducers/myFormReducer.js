import {/*COPY, PASTE,*/ RESET, prefix} from '../type'

const initialValues = Object.freeze({
    credit_card: '0000 0000 0000 0000',
    cvv: '000'
})

// seems that redux form check if values have changed since last restore
// so we have to create a fake information forceUpdate when we restore twice the same information
let forceUpdate = 0;
// lastData can be the last data saved or null
const myFormReducer = (state = {lastData: null, dataForLoading: initialValues}, action, root) => {
    // win time by returning the state if it does not concern our reducer
    if(action.type.slice(0,prefix.length) !== prefix){
        return state
    }
    switch (action.type) {
            case RESET: 
                console.log('RESET')
                return {
                    ...state,
                    dataForLoading: {...initialValues,forceUpdate: ++forceUpdate},
                }
            default:
                console.error('Default' + action.type)
                return state;
    }
};
export default myFormReducer;