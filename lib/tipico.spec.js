"use strict";

const _require = require('./formator'),
      getSolution = _require.getSolution,
      getMaxLength = _require.getMaxLength;

describe('1 cell', function () {
  it('should works for 1 digit', function () {
    expect(getSolution([4])).toEqual(`+-+
|4|
+-+`);
  });
  it('should works for 5 digits', function () {
    expect(getSolution([12345])).toEqual(`+-----+
|12345|
+-----+`);
  });
});
describe('multiple cells', function () {
  it('expect maxLength to works', function () {
    expect(getMaxLength([4, 35, 80, 123])).toEqual(3);
  });
  it('should works for 5 digits', function () {
    expect(getSolution([4, 35, 80, 123])).toEqual(`+---+---+---+---+
|  4| 35| 80|123|
+---+---+---+---+`);
  });
});
describe('multiple lines', function () {
  it('do not break the line', function () {
    expect(getSolution([4, 35, 80, 123, 12345, 44, 8, 5], 10)).toEqual(`+-----+-----+-----+-----+-----+-----+-----+-----+
|    4|   35|   80|  123|12345|   44|    8|    5|
+-----+-----+-----+-----+-----+-----+-----+-----+`);
  });
  it('not a square', function () {
    expect(getSolution([4, 35, 80, 123, 12345, 44, 8, 5, 24, 3], 4)).toEqual(`+-----+-----+-----+-----+
|    4|   35|   80|  123|
+-----+-----+-----+-----+
|12345|   44|    8|    5|
+-----+-----+-----+-----+
|   24|    3|
+-----+-----+`);
  });
  it('square', function () {
    expect(getSolution([4, 35, 80, 123, 12345, 44, 8, 5, 24, 3, 22, 35], 4)).toEqual(`+-----+-----+-----+-----+
|    4|   35|   80|  123|
+-----+-----+-----+-----+
|12345|   44|    8|    5|
+-----+-----+-----+-----+
|   24|    3|   22|   35|
+-----+-----+-----+-----+`);
  }); // I allow the user to enter string, this way we don't have to control the user input

  it('do not break the line and works with string', function () {
    expect(getSolution(['23242', ' 2aaa'], '2')).toEqual(`+-----+-----+
|23242| 2aaa|
+-----+-----+`);
  }); // I allow the user to enter string, this way we don't have to control the user input

  it('one long column', function () {
    expect(getSolution(['1', '3323', '4'], '1')).toEqual(`+----+
|   1|
+----+
|3323|
+----+
|   4|
+----+`);
  });
});