// do not put / at the end
const serverRoot = 'http://127.0.0.1:5555/serverRoot'
const router = {
	pay: serverRoot + '/api/pay'
}
const getConfig = function(){
	return {router}
}
export default getConfig