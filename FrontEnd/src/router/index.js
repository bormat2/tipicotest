import React from 'react';
import TipicoPage from '../pages/Tipico/TipicoPage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import App from '../App';


class Index extends React.Component {
  render() {
    return (
      <Router>
        <App>
          <Switch>
            <Route exact path="/" component={TipicoPage} />
          </Switch>
        </App>
      </Router>
    );
  }
}
export default Index;
