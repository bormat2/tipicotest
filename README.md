# [TipicoTest](https://gitlab.com/bormat2/tipicotest) &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](#)

The goal of this project is to format array of numbers, if we choose a number of column of 4

here is the result expected
<pre>
+-----+-----+-----+-----+
|    4|   35|   80|  123|
+-----+-----+-----+-----+
|12345|   44|    8|    5|
+-----+-----+-----+-----+
|   24|    3|
+-----+-----+
</pre>
The complete specification can be found in Tipico FE Exercise (Frontend Developer).docx


# Installation
## Get the project
`git clone https://gitlab.com/bormat2/carmatest.git`

## Install dependencies
  In the root folder run :
`npm i`

## Run test
Before using the program run tests to check if everything is ok:
`npm run test`

# Console program
To run the console program just do :
`npm start`

# React program
To run the graphical program go in FrontEnd
`cd FrontEnd`

install dependencies
`npm  i`

and run react:
`npm start`
