"use strict";

var _formator = require("./formator");

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const vals = {};
rl.question('Enter numbers separate with coma, for example: 3,4 and press enter: \n', answer => {
  vals.solution = answer.split(',');

  const rec = function () {
    rl.question('How many cells do you want by line: \n', function (answer) {
      if (isNaN(answer)) {
        rec();
        return;
      }

      (0, _formator.solution)(vals.solution, answer);
      rl.close();
    });
  };

  rec();
});