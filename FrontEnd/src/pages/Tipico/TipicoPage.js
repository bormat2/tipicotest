import React from 'react';
import TipicoForm from './TipicoForm';
// import { reset } from 'redux-form';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
// import getConfig from '../../getConfig';
import {connectWithStyle} from '../../utils.js'
import {getSolution} from '../../../../src/formator.js'

const styles = theme => {
    const paddingLeft = theme.normalPadding+'px'
    const style = {
        paddingLeft: {paddingLeft}
    }
    return style 
}

export class TipicoPage extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.sendMyCard = this.sendMyCard.bind(this);
        this.state = { 
            notify: false,
            message: '',
            error: false,
            dialogOpen: false,
         };
    }

    sendMyCard(values) {
        this.setState({solution : getSolution((values.digits||'').split(','), values.maxCells)})
        
    }

    handleClose(){
        this.setState({dialogOpen : false})
    }

    openDialog(message){
        this.setState({dialogOpen : true, message})
    }

    render() {
        return (
            <div>
                <Card>
                    <CardContent>
                        <h1>Tipico test page</h1>
                    </CardContent>
                    <TipicoForm
                        onSubmit={this.sendMyCard}
                        onCancel={() => this.redirectToProjectInfoList()}
                        editMode
                    />
                </Card>
                {this.state.solution && <pre>{this.state.solution}</pre>}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const id = ownProps.match.params.id;
    const form = state.form.reduxForm_srtCorrector
    return {
        id: id,
        formVal: form && form.values
    };
}

export default connectWithStyle({comp: TipicoPage, mapStateToProps, styles});
