"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.solution = exports.getMaxLength = exports.getSolution = void 0;

/**
*	join an array but also add the separator at the start and the end of the string
*/
const joinAndLat = (arr, separator) => separator + arr.join(separator) + separator;
/**
* create the line containing numbers, each cell must have the same length
* array is an array of number, maxLength is the length that our cells must have
* example: for the array input [4,35,80|123|12345,44, 8, 5]
* we will return "|    4|   35|   80|  123|12345|   44|    8|    5|"
*/


const arrayToLine = (array, maxLength) => joinAndLat(array.map(number => {
  const nbRepeat = maxLength - ('' + number).length || 0;
  return ' '.repeat(nbRepeat) + number;
}), '|');
/** 
*	Return the length of the longuer number (can also work with string)
*/


const getMaxLength = function (array) {
  return array.reduce((max, number) => ('' + number).length > max.length ? '' + number : max, '').length;
};
/**
*	get the line between the line of numbers
*   less is a string like "---" 
*   return a string like this "+---+---+---+"
*/


exports.getMaxLength = getMaxLength;

const getLineSeparator = function (lg, less) {
  return joinAndLat(Array(+lg).fill(less), '+');
};
/** 
* for an array input like [4, 35, 80, 123, 12345, 44, 8, 5,24,3] and _k1 = 4 we will return 
* 
* +-----+-----+-----+-----+
* |    4|   35|   80|  123|
* +-----+-----+-----+-----+
* |12345|   44|    8|    5|
* +-----+-----+-----+-----+
* |   24|    3|
* +-----+-----+
*
* Where _k1 is the number max of cells by line, each cell has the same length
* if _k1 is null we will create only one line
*/


const getSolution = function (array, _k1) {
  const _k = +(_k1 || array.length); // what is the length of the longest number


  const maxLength = getMaxLength(array);
  const less = '-'.repeat(maxLength);
  let res = []; // the line that appear at first line, at end line and between lines that contain number
  // like +------+

  const lineSeparatorEnd = getLineSeparator(array.length % _k || _k, less); // when there is only one line the last line separator is the same that the first

  const lineSeparator = (array.length <= _k ? lineSeparatorEnd : getLineSeparator(_k, less)) + '\n';

  for (let i = 0; i < array.length; i += _k) {
    res.push(arrayToLine(array.slice(i, i + _k), maxLength));
  }

  return lineSeparator + res.join('\n' + lineSeparator) + '\n' + lineSeparatorEnd;
};

exports.getSolution = getSolution;

const solution = function (array, k) {
  process.stdout.write(getSolution(array, k));
};

exports.solution = solution;